package org.darkrealm.factions.darkrealmenchants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.rit.sucy.EnchantmentAPI;
import com.rit.sucy.service.ERomanNumeral;

public class EnchantingManager implements Listener {

    public static final ItemStack BOTTLE;

    public static final PurchasableItem[] INV;

    public static final String NAME = "Enchantment Books";

    public static final String BOTTLE_GUI = "Bottle Experience";

    public static final String[] BOOK_LORE = { "", ChatColor.GRAY + "Pick this up and click",
            ChatColor.GRAY + "an item to enchant it.", "" };

    public static final String[] BOTTLE_LORE = { ChatColor.WHITE + "Contains: 0 xp",
            ChatColor.GRAY + "Left click while holding", ChatColor.GRAY + "this to bottle experience.",
            ChatColor.GRAY + "Right click to consume." };

    public static final String BOTTLE_NAME = ChatColor.GREEN + "Experience Flask";

    public static final String CORRUPT_TAG = ChatColor.DARK_RED + "" + ChatColor.BOLD + "CORRUPT";

    public static final HashMap<EnchantRarity, ArrayList<EnchantLevel>> enchants = new HashMap<EnchantRarity, ArrayList<EnchantLevel>>();

    public final DarkRealmEnchants pl;

    static {

        BOTTLE = new ItemStack(Material.EXP_BOTTLE);
        ItemMeta meta = BOTTLE.getItemMeta();
        List<String> lore = new ArrayList<String>();
        for (String s : BOTTLE_LORE)
            lore.add(s);
        meta.setLore(lore);
        meta.setDisplayName(BOTTLE_NAME);
        BOTTLE.setItemMeta(meta);

        INV = new PurchasableItem[] { PurchasableItem.fromRarity(EnchantRarity.COMMON),
                PurchasableItem.fromRarity(EnchantRarity.UNCOMMON), PurchasableItem.fromRarity(EnchantRarity.RARE),
                PurchasableItem.fromRarity(EnchantRarity.MYTHICAL), PurchasableItem.fromRarity(EnchantRarity.LEGENDARY),
                PurchasableItem.fromItem(BOTTLE, 1000), PurchasableItem.fromScroll(Scroll.PURITY, 50000),
                PurchasableItem.fromScroll(Scroll.ETERNITY, 50000), PurchasableItem.fromScroll(Scroll.ARCHIVE, 25000) };

        for (EnchantRarity r : EnchantRarity.values())
            enchants.put(r, new ArrayList<EnchantLevel>());
    }

    public EnchantingManager(DarkRealmEnchants pl) {
        this.pl = pl;
    }

    public void buyGui(Player p) {
        Inventory inv = Bukkit.createInventory(p, 9, NAME);
        for (int i = 0; i < INV.length; i++)
            inv.setItem(i, INV[i].getDisplayItem(this));
        p.openInventory(inv);
    }

    public void bottleGui(Player p) {
        ItemStack s = p.getInventory().getItemInMainHand();
        p.getInventory().setItemInMainHand(null);
        Inventory i = Bukkit.createInventory(p, InventoryType.ANVIL, BOTTLE_GUI);
        s.getItemMeta().setDisplayName("0");
        i.setItem(0, s);
        p.openInventory(i);
    }

    public void buy(Player p, PurchasableItem item) {
        ExperienceManager m = new ExperienceManager(p);
        if (m.getTotalExperience() < item.getCost(this))
            p.sendMessage(ChatColor.DARK_RED + "You need " + (item.getCost(this) - m.getTotalExperience())
                    + " more experience points to purchase this item!");
        else {
            p.getInventory().addItem(item.getPurchaseItem(this));
            m.setTotalExperience(m.getTotalExperience() - item.getCost(this));
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getView().getTopInventory() != null && e.getView().getTopInventory().getName() != null) {
            if (e.getView().getTopInventory().getName().equals(NAME)) {
                e.setCancelled(true);
                if (e.getCurrentItem() != null && e.getClickedInventory().equals(e.getView().getTopInventory())
                        && e.getSlot() < INV.length) {
                    buy((Player) e.getWhoClicked(), INV[e.getSlot()]);
                }
            } else if (e.getView().getTopInventory().getName().equals(BOTTLE_GUI)) {
                if (e.getClickedInventory().equals(e.getView().getTopInventory())) {
                    System.out.println(e.getRawSlot());
                    switch (e.getSlot()) {
                    case 0:
                        e.getCurrentItem().getItemMeta().setDisplayName(BOTTLE.getItemMeta().getDisplayName());
                        e.setCancelled(false);
                        e.getWhoClicked().closeInventory();
                        break;
                    case 2:
                        String s = e.getCurrentItem().getItemMeta().getDisplayName();
                        e.setCancelled(true);
                        e.getView().getTopInventory().getItem(0).getItemMeta()
                                .setDisplayName(BOTTLE.getItemMeta().getDisplayName());
                        try {
                            int amt = Integer.parseInt(s);
                            ExperienceManager m = new ExperienceManager((Player) e.getWhoClicked());
                            if (m.getTotalExperience() < amt)
                                e.getWhoClicked().sendMessage(ChatColor.RED + "You don't have enough experience!");
                            else {
                                m.setTotalExperience(m.getTotalExperience() - amt);
                                ItemStack item = e.getView().getTopInventory().getItem(0);
                                ItemMeta meta = item.getItemMeta();
                                List<String> lore = meta.getLore();
                                int current = Integer.parseInt(lore.get(0).substring(
                                        (ChatColor.WHITE + "Contains: ").length(), lore.get(0).length() - 3));
                                lore.set(0, ChatColor.WHITE + "Contains: " + (amt + current) + " xp");
                                meta.setLore(lore);
                                item.setItemMeta(meta);
                                e.getView().getTopInventory().setItem(0, item);
                            }
                        } catch (NumberFormatException ex) {
                            e.getWhoClicked().sendMessage(ChatColor.RED + "That's not a number...");
                        }
                        e.getWhoClicked().closeInventory();
                        break;
                    default:
                        e.setCancelled(true);
                        break;
                    }
                } else
                    e.setCancelled(true);
            }
        }
        if (e.getAction() == InventoryAction.SWAP_WITH_CURSOR && e.getCurrentItem() != null
                && e.getWhoClicked().getItemOnCursor() != null) {
            EnchantLevel enchant = getBookEnchant(e.getWhoClicked().getItemOnCursor());
            if (enchant != null) {
                e.setCancelled(true);
                boolean worked = false;
                if (enchant.enchant.appliesTo.materials.contains(e.getCurrentItem().getType())) {
                    if (!EnchantmentAPI.itemHasEnchantment(e.getCurrentItem(), enchant.enchant.name())) {
                        if (!isCorrupt(e.getCurrentItem())) {
                            apply(e.getWhoClicked().getItemOnCursor(), e.getCurrentItem(), enchant,
                                    ((Player) e.getWhoClicked()));
                            e.getWhoClicked().setItemOnCursor(null);
                            worked = true;
                        } else
                            e.getWhoClicked().sendMessage(ChatColor.RED + "That item is corrupt!");
                    } else
                        e.getWhoClicked().sendMessage(ChatColor.RED + "That item already has that enchantment!");
                } else
                    e.getWhoClicked().sendMessage(ChatColor.RED + "That book does not apply to that item!");
                if (!worked)
                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getEyeLocation(), Sound.BLOCK_ANVIL_LAND,
                            1, 1);
            } else {
                boolean scroll = true;
                ItemStack item = null;
                String message = ChatColor.RED + "That item is already ";
                Sound sound = null;
                float pitch = 0.5f;
                if (e.getWhoClicked().getItemOnCursor().equals(Scroll.PURITY.item)) {
                    if (!isPurified(e.getCurrentItem())) {
                        item = purify(e.getCurrentItem());
                        sound = Sound.ENTITY_PLAYER_LEVELUP;
                    } else
                        message += "purified!";
                } else if (e.getWhoClicked().getItemOnCursor().equals(Scroll.ARCHIVE.item)) {
                    if (!isArchived(e.getCurrentItem())) {
                        item = archive(e.getCurrentItem());
                        sound = Sound.BLOCK_CHEST_LOCKED;
                    } else
                        message += "archived!";
                } else if (e.getWhoClicked().getItemOnCursor().equals(Scroll.ETERNITY.item)) {
                    if (!isPreserved(e.getCurrentItem())) {
                        item = preserve(e.getCurrentItem());
                        sound = Sound.BLOCK_NOTE_PLING;
                    } else
                        message += "purified!";
                } else
                    scroll = false;
                if (scroll) {
                    e.setCancelled(true);
                    if (item == null) {
                        e.getWhoClicked().sendMessage(message);
                        ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getEyeLocation(),
                                Sound.BLOCK_ANVIL_LAND, 1, 1);
                    } else {
                        e.setCurrentItem(item);
                        e.getWhoClicked().setItemOnCursor(null);
                        ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getEyeLocation(), sound, 1, pitch);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        ArrayList<ItemStack> archive = new ArrayList<ItemStack>();
        for (int i = e.getDrops().size() - 1; i >= 0; i--)
            if (isArchived(e.getDrops().get(i)))
                archive.add(e.getDrops().remove(i));
        Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
            public void run() {
                e.getEntity().getInventory().addItem(archive.toArray(new ItemStack[] {}));
            }
        });
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (isBottle(e.getItem())) {
            switch (e.getAction()) {
            case RIGHT_CLICK_AIR:
            case RIGHT_CLICK_BLOCK:
                e.setCancelled(true);
                unbottle(e.getPlayer());
                break;
            // TODO change back
            /*
             * case LEFT_CLICK_AIR: case LEFT_CLICK_BLOCK: e.setCancelled(true);
             * bottleGui(e.getPlayer()); break;
             */
            default:
                break;
            }
        }
    }

    @EventHandler
    public void onClickEntity(PlayerInteractEntityEvent e) {
        if (isBottle(e.getPlayer().getInventory().getItemInMainHand())) {
            e.setCancelled(true);
            unbottle(e.getPlayer());
        }
    }

    public void unbottle(Player p) {
        ItemStack stack = p.getInventory().getItemInMainHand();
        if (!isBottle(stack))
            return;
        ItemMeta meta = stack.getItemMeta();
        List<String> lore = meta.getLore();
        String s = lore.get(0);
        int amt = Integer.parseInt(s.substring((ChatColor.WHITE + "Contains: ").length(), s.length() - " xp".length()));
        ExperienceManager m = new ExperienceManager(p);
        m.setTotalExperience(m.getTotalExperience() + amt);
        lore.set(0, ChatColor.WHITE + "Contains: 0 xp");
        meta.setLore(lore);
        stack.setItemMeta(meta);
        p.getInventory().setItemInMainHand(stack);
    }

    public boolean isBottle(ItemStack s) {
        if (s == null || s.getType() != BOTTLE.getType())
            return false;
        if (s.getItemMeta().getLore() == null)
            return false;
        if (!BOTTLE.getItemMeta().getDisplayName().equals(s.getItemMeta().getDisplayName()))
            return false;
        if (s.getItemMeta().getLore() == null
                || s.getItemMeta().getLore().size() != BOTTLE.getItemMeta().getLore().size())
            return false;
        for (int i = BOTTLE.getItemMeta().getLore().size() - 1; i >= 1; i--)
            if (!BOTTLE.getItemMeta().getLore().get(i).equals(s.getItemMeta().getLore().get(i)))
                return false;
        if (!s.getItemMeta().getLore().get(0).matches(ChatColor.WHITE + "Contains: \\d+ xp"))
            return false;
        return true;
    }

    public EnchantLevel getBookEnchant(ItemStack stack) {
        if (!isEnchantBook(stack))
            return null;
        String name = stack.getItemMeta().getDisplayName().substring(4);
        return new EnchantLevel((DREnchant) EnchantmentAPI.getEnchantment(name.substring(0, name.lastIndexOf(" "))),
                ERomanNumeral.getValueOf(name.substring(name.lastIndexOf(" ") + 1)));
    }

    public ItemStack apply(ItemStack book, ItemStack toEnchant, EnchantLevel enchant, Player enchanter) {
        String lore = book.getItemMeta().getLore().get(7);
        if (!isPurified(toEnchant) && Math.random() < Double.parseDouble(
                lore.substring((ChatColor.RED + "Chance of corrupting item: ").length(), lore.length() - 1)) / 100.0) {
            corrupt(toEnchant);
            enchanter.playSound(enchanter.getEyeLocation(), Sound.ENTITY_ZOMBIE_BREAK_DOOR_WOOD, 1, 1);
        } else {
            enchant.enchant.addToItem(toEnchant, enchant.level);
            enchanter.playSound(enchanter.getEyeLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
        }
        return toEnchant;
    }

    public boolean isCorrupt(ItemStack item) {
        return item != null && item.getItemMeta() != null && item.getItemMeta().getLore() != null
                && item.getItemMeta().getLore().contains(CORRUPT_TAG);
    }

    public boolean isPurified(ItemStack item) {
        return item != null && item.getItemMeta() != null && item.getItemMeta().getLore() != null
                && item.getItemMeta().getLore().contains(Scroll.PURITY.tag);
    }

    public boolean isArchived(ItemStack item) {
        return item != null && item.getItemMeta() != null && item.getItemMeta().getLore() != null
                && item.getItemMeta().getLore().contains(Scroll.ARCHIVE.tag);
    }

    public boolean isPreserved(ItemStack item) {
        return item != null && item.getItemMeta() != null && item.getItemMeta().getLore() != null
                && item.getItemMeta().getLore().contains(Scroll.ETERNITY.tag);
    }

    public ItemStack corrupt(ItemStack item) {
        item = item.clone();
        ItemMeta m = item.getItemMeta();
        List<String> lore = m.getLore() != null ? m.getLore() : new ArrayList<String>();
        lore.add(0, CORRUPT_TAG);
        m.setLore(lore);
        item.setItemMeta(m);
        return item;
    }

    public ItemStack purify(ItemStack item) {
        item = item.clone();
        ItemMeta m = item.getItemMeta();
        List<String> lore = m.getLore() != null ? m.getLore() : new ArrayList<String>();
        lore.remove(CORRUPT_TAG);
        lore.add(Scroll.PURITY.tag);
        m.setLore(lore);
        item.setItemMeta(m);
        return item;
    }

    public ItemStack archive(ItemStack item) {
        item = item.clone();
        ItemMeta m = item.getItemMeta();
        List<String> lore = m.getLore() != null ? m.getLore() : new ArrayList<String>();
        lore.add(Scroll.ARCHIVE.tag);
        m.setLore(lore);
        item.setItemMeta(m);
        return item;
    }

    public ItemStack preserve(ItemStack item) {
        item = item.clone();
        ItemMeta m = item.getItemMeta();
        List<String> lore = m.getLore() != null ? m.getLore() : new ArrayList<String>();
        lore.add(Scroll.ETERNITY.tag);
        m.setLore(lore);
        item.setItemMeta(m);
        new ItemBuilder(item).setUnbreakable(true).hideFlags(4).build();
        return item;
    }

    public boolean isEnchantBook(ItemStack stack) {
        if (stack == null || stack.getType() != Material.ENCHANTED_BOOK)
            return false;
        List<String> lore = stack.getItemMeta().getLore();
        if (lore == null || lore.size() != BOOK_LORE.length + 4 || !lore.get(0).endsWith(" Enchantment")
                || !lore.get(5).startsWith(ChatColor.BLUE + "Can be applied to any ") || !lore.get(6).equals("")
                || !lore.get(7).matches(ChatColor.RED + "Chance of corrupting item: \\d{1,2}\\.\\d%"))
            return false;
        for (int i = 0; i < BOOK_LORE.length; i++)
            if (!BOOK_LORE[i].equals(lore.get(i + 1)))
                return false;
        String name = stack.getItemMeta().getDisplayName().substring(4);
        if (!name.matches("[a-zA-Z ]*[a-zA-Z]+ [MDCLXVI]+"))
            return false;
        return true;
    }

    public ItemStack randomBook(EnchantRarity r) {
        ArrayList<EnchantLevel> alel = enchants.get(r);
        EnchantLevel el = alel.get((int) (Math.random() * alel.size()));
        ItemStack stack = new ItemStack(Material.ENCHANTED_BOOK);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(
                r.displayName.substring(0, 4) + el.enchant.name() + " " + ERomanNumeral.numeralOf(el.level));
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(r.displayName.substring(0, 2) + r.displayName.substring(4) + " Enchantment");
        for (String s : BOOK_LORE)
            lore.add(s);
        lore.add(ChatColor.BLUE + "Can be applied to any " + el.enchant.appliesTo.name);
        lore.add("");
        lore.add(String.format(ChatColor.RED + "Chance of corrupting item: %.1f%%", Math.random() * 20));
        meta.setLore(lore);
        stack.setItemMeta(meta);
        return stack;
    }

    public void register(DREnchant enchantment) {
        EnchantRarity[] rarities = enchantment.rarities();
        for (int i = 0; i < rarities.length; i++)
            enchants.get(rarities[i]).add(new EnchantLevel(enchantment, i + 1));
    }

}