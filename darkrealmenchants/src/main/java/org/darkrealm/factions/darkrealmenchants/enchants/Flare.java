package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.DarkRealmEnchants;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

import com.rit.sucy.EnchantmentAPI;

public class Flare extends DREnchant {

    public static final String NAME = "Flare";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.COMMON, EnchantRarity.UNCOMMON, EnchantRarity.RARE };

    public Flare() {
        super(NAME, ItemType.BOW, RARITIES);
    }

    public void onEntityDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Arrow && e.getEntity() instanceof LivingEntity && EnchantmentAPI
                .getAllEnchantments(DarkRealmEnchants.getBow((Arrow) e.getDamager())).containsKey(this)) {
            int level = EnchantmentAPI.getAllEnchantments(DarkRealmEnchants.getBow((Arrow) e.getDamager())).get(this);
            if (!(e.getEntity() instanceof Player) || Math.random() < 0.2 * level)
                e.getEntity().setFireTicks((int) (80 * (level * (Math.random() / 2 + 0.75))));
        }
    }

}