package org.darkrealm.factions.darkrealmenchants.enchants;

import java.util.Random;
import java.util.UUID;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.entity.Player;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Vitality extends DREnchant {

    public Vitality(String name, ItemType appliesTo, EnchantRarity[] rarities) {
        super(name, appliesTo, rarities);
    }

    public static final AttributeModifier[][] MODIFIERS;

    public static final String[] ATTRIBUTES = { "DarkRealm Vitality Boots Health Modifier",
            "DarkRealm Vitality Leggings Health Modifier", "DarkRealm Vitality Chestplate Health Modifier",
            "DarkRealm Vitality Helmet Health Modifier" };

    static {
        Random random = new Random(10986325);
        MODIFIERS = new AttributeModifier[4][];
        for (int slot = 0; slot < MODIFIERS.length; slot++)
            MODIFIERS[slot] = new AttributeModifier[] { modifier(0, randomUUID(random), slot),
                    modifier(1, randomUUID(random), slot), modifier(2, randomUUID(random), slot),
                    modifier(3, randomUUID(random), slot) };
    }

    public static AttributeModifier modifier(int amount, UUID uuid, int slot) {
        return new AttributeModifier(uuid, ATTRIBUTES[slot], amount * 2, Operation.ADD_NUMBER);
    }

    public static final String NAME = "Vitality";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.MYTHICAL, EnchantRarity.LEGENDARY,
            EnchantRarity.LEGENDARY };

    public Vitality() {
        super(NAME, ItemType.CHESTPLATE, RARITIES);
    }

    public void update(Player p) {
        for (int i = 0; i < 4; i++) {
            if (hasItem(p, 0))
                this.replacePlayerModifier(p, Attribute.GENERIC_MAX_HEALTH, ATTRIBUTES[i], MODIFIERS[i][level(p, i)]);
            else
                this.removePlayerModifier(p, Attribute.GENERIC_MAX_HEALTH, ATTRIBUTES[i]);
        }
    }

    @Override
    public void applyEquipEffect(Player p, int level) {
        update(p);
    }

    @Override
    public void applyUnequipEffect(Player p, int level) {
        update(p);
    }

}