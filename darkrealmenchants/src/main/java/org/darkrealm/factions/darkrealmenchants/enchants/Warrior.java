package org.darkrealm.factions.darkrealmenchants.enchants;

import java.util.Random;
import java.util.UUID;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Warrior extends DREnchant {

    public static final String NAME = "Warrior";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.LEGENDARY, EnchantRarity.LEGENDARY,
            EnchantRarity.LEGENDARY };

    public static final AttributeModifier[] MODIFIERS;

    public static final String ATTRIBUTE = "DarkRealm Warrior Damage Modifier";

    static {
        Random random = new Random(125310);
        MODIFIERS = new AttributeModifier[] { modifier(0, randomUUID(random)), modifier(1, randomUUID(random)),
                modifier(2, randomUUID(random)), modifier(3, randomUUID(random)), modifier(4, randomUUID(random)) };
    }

    public static AttributeModifier modifier(int amount, UUID uuid) {
        return new AttributeModifier(uuid, ATTRIBUTE, 0.2 * amount, Operation.MULTIPLY_SCALAR_1);
    }

    public Warrior() {
        super(NAME, ItemType.ALL_ARMOR, RARITIES);
    }

    public void updatePlayer(Player p) {
        if (level(p, 2) > 0 && p.getHealth() <= 8) {
            replacePlayerModifier(p, Attribute.GENERIC_ATTACK_DAMAGE, ATTRIBUTE, MODIFIERS[level(p, 2)]);
        } else
            removePlayerModifier(p, Attribute.GENERIC_ATTACK_DAMAGE, ATTRIBUTE);
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player)
            updatePlayer((Player) e.getEntity());
    }

    @EventHandler
    public void onHeal(EntityRegainHealthEvent e) {
        if (e.getEntity() instanceof Player)
            updatePlayer((Player) e.getEntity());
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        updatePlayer(e.getPlayer());
    }
}