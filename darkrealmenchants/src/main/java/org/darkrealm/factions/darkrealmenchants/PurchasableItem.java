package org.darkrealm.factions.darkrealmenchants;

import org.bukkit.inventory.ItemStack;

public interface PurchasableItem {

    public static PurchasableItem fromRarity(EnchantRarity r) {
        return new EnchantBook(r);
    }

    public static PurchasableItem fromItem(ItemStack item, int cost) {
        return new ItemWithCost(item, cost);
    }

    public static PurchasableItem fromScroll(Scroll scroll, int cost) {
        return new ScrollBook(scroll, cost);
    }

    public ItemStack getDisplayItem(EnchantingManager manager);

    public ItemStack getPurchaseItem(EnchantingManager manager);

    public int getCost(EnchantingManager manager);

}