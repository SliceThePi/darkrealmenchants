package org.darkrealm.factions.darkrealmenchants.enchants;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.DarkRealmEnchants;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Fury extends DREnchant {

    public static final String NAME = "Fury";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.MYTHICAL, EnchantRarity.MYTHICAL,
            EnchantRarity.MYTHICAL, EnchantRarity.LEGENDARY };

    public static final AttributeModifier[] MODIFIERS;

    public static final String ATTRIBUTE = "DarkRealm Fury Attack Speed Modifier";

    private final DarkRealmEnchants pl;

    static {
        Random random = new Random(4767943);
        MODIFIERS = new AttributeModifier[] { modifier(0, randomUUID(random)), modifier(1, randomUUID(random)),
                modifier(2, randomUUID(random)), modifier(3, randomUUID(random)), modifier(4, randomUUID(random)) };
    }

    public static AttributeModifier modifier(int amount, UUID uuid) {
        return new AttributeModifier(uuid, ATTRIBUTE, (int) (Math.pow(1.1487, amount) * 100) / 100.0 - 1,
                Operation.MULTIPLY_SCALAR_1);
    }

    public Fury() {
        super(NAME, ItemType.ALL_MELEE, RARITIES);
        pl = (DarkRealmEnchants) Bukkit.getPluginManager().getPlugin("DarkRealmEnchants");
    }

    public void updatePlayer(Player p) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
            @Override
            public void run() {
                if (level(p, 4) > 0) {
                    replacePlayerModifier(p, Attribute.GENERIC_ATTACK_SPEED, ATTRIBUTE, MODIFIERS[level(p, 4)]);
                } else
                    removePlayerModifier(p, Attribute.GENERIC_ATTACK_SPEED, ATTRIBUTE);
            }
        });
    }

    @EventHandler
    public void onPlayerInteractInventory(InventoryClickEvent e) {
        updatePlayer((Player) e.getWhoClicked());
    }

    @EventHandler
    public void onPlayerSwitchItem(PlayerItemHeldEvent e) {
        updatePlayer(e.getPlayer());
    }
}