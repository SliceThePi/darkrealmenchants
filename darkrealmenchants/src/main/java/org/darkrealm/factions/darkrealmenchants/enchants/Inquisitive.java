package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDeathEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Inquisitive extends DREnchant {

    public static final String NAME = "Inquisitive";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.LEGENDARY, EnchantRarity.LEGENDARY,
            EnchantRarity.LEGENDARY, EnchantRarity.LEGENDARY };

    public Inquisitive() {
        super(NAME, ItemType.ALL_MELEE, RARITIES);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMobDeath(EntityDeathEvent e) {
        if (e.getEntity().getKiller() != null && hasItem(e.getEntity().getKiller(), 4)) {
            e.setDroppedExp(e.getDroppedExp() + e.getDroppedExp() * level(e.getEntity().getKiller(), 4));
        }
    }
}