package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;
import org.darkrealm.factions.darkrealmenchants.TickingEnchant;

public class Scuba extends DREnchant implements TickingEnchant {

    public static final String NAME = "Scuba";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.LEGENDARY };

    public Scuba() {
        super(NAME, ItemType.HELMET, RARITIES);
    }

    @Override
    public void tick() {
        for (Player p : Bukkit.getOnlinePlayers())
            if (hasItem(p, 3) && p.getRemainingAir() < p.getMaximumAir())
                p.setRemainingAir(p.getMaximumAir());
    }
}