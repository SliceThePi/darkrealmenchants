package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Rations extends DREnchant {

    public static final String NAME = "Rations";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.COMMON };

    public Rations() {
        super(NAME, ItemType.HELMET, RARITIES);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLoseHunger(FoodLevelChangeEvent e) {
        if (level((Player) e.getEntity(), 3) != 0 && e.getFoodLevel() < 8
                && ((Player) e.getEntity()).getFoodLevel() >= 8)
            e.setFoodLevel(10);
    }
}