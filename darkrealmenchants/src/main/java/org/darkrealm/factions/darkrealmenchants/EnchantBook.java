package org.darkrealm.factions.darkrealmenchants;

import org.bukkit.inventory.ItemStack;

public class EnchantBook implements PurchasableItem {

    public final EnchantRarity rarity;

    public EnchantBook(EnchantRarity rarity) {
        this.rarity = rarity;
    }

    @Override
    public ItemStack getDisplayItem(EnchantingManager manager) {
        return rarity.stack;
    }

    @Override
    public ItemStack getPurchaseItem(EnchantingManager manager) {
        return manager.randomBook(rarity);
    }

    @Override
    public int getCost(EnchantingManager manager) {
        return rarity.cost;
    }

}