package org.darkrealm.factions.darkrealmenchants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.darkrealm.factions.darkrealmenchants.enchants.Blaze;
import org.darkrealm.factions.darkrealmenchants.enchants.Bulwark;
import org.darkrealm.factions.darkrealmenchants.enchants.Caution;
import org.darkrealm.factions.darkrealmenchants.enchants.Clarity;
import org.darkrealm.factions.darkrealmenchants.enchants.Dangerguard;
import org.darkrealm.factions.darkrealmenchants.enchants.Decapitation;
import org.darkrealm.factions.darkrealmenchants.enchants.Deft;
import org.darkrealm.factions.darkrealmenchants.enchants.Escape;
import org.darkrealm.factions.darkrealmenchants.enchants.Flare;
import org.darkrealm.factions.darkrealmenchants.enchants.Force;
import org.darkrealm.factions.darkrealmenchants.enchants.Fury;
import org.darkrealm.factions.darkrealmenchants.enchants.Gearshift;
import org.darkrealm.factions.darkrealmenchants.enchants.Inquisitive;
import org.darkrealm.factions.darkrealmenchants.enchants.Jelly;
import org.darkrealm.factions.darkrealmenchants.enchants.Keen;
import org.darkrealm.factions.darkrealmenchants.enchants.Leech;
import org.darkrealm.factions.darkrealmenchants.enchants.Obliteration;
import org.darkrealm.factions.darkrealmenchants.enchants.ObsidianShield;
import org.darkrealm.factions.darkrealmenchants.enchants.Opulent;
import org.darkrealm.factions.darkrealmenchants.enchants.Piercing;
import org.darkrealm.factions.darkrealmenchants.enchants.Rations;
import org.darkrealm.factions.darkrealmenchants.enchants.Scuba;
import org.darkrealm.factions.darkrealmenchants.enchants.Spines;
import org.darkrealm.factions.darkrealmenchants.enchants.Toxic;
import org.darkrealm.factions.darkrealmenchants.enchants.Vitality;
import org.darkrealm.factions.darkrealmenchants.enchants.Warrior;

import com.rit.sucy.EnchantPlugin;
import com.rit.sucy.EnchantmentAPI;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;

public class DarkRealmEnchants extends EnchantPlugin implements Listener {

    public PluginManager pluginManager;

    public ArrayList<Listener> toListen = new ArrayList<Listener>();

    public EnchantingManager manager = new EnchantingManager(this);

    private ArrayList<TickingEnchant> tickingEnchants = new ArrayList<TickingEnchant>();

    @Override
    public void onEnable() {
        pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(this, this);
        pluginManager.registerEvents(manager, this);
        for (Listener l : toListen)
            pluginManager.registerEvents(l, this);
        CitizensAPI.registerEvents(this);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                for (TickingEnchant e : tickingEnchants)
                    e.tick();
            }
        }, 1, 1);

        getLogger().info("Loaded!");
    }

    public void listenAndRegister(DREnchant... enchantments) {
        for (DREnchant enchantment : enchantments) {
            if (enchantment instanceof TickingEnchant)
                tickingEnchants.add((TickingEnchant) enchantment);
            toListen.add(enchantment);
            EnchantmentAPI.registerCustomEnchantment(enchantment);
            manager.register(enchantment);
        }
    }

    @Override
    public void registerEnchantments() {
        Clarity c = new Clarity();
        listenAndRegister(new Gearshift(), new Leech(), new Blaze(), new Obliteration(), new ObsidianShield(),
                new Toxic(), new Fury(), new Vitality(), new Spines(), new Rations(), new Warrior(), new Escape(),
                new Jelly(), new Inquisitive(), new Dangerguard(), new Keen(), new Scuba(), new Bulwark(), c,
                new Opulent(c), new Deft(), new Force(), new Flare(), new Piercing(), new Decapitation(),
                new Caution());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        switch (command.getLabel()) {
        case "bottlexp":
            if (args.length == 1) {
                try {
                    if (!(sender instanceof Player)) {
                        sender.sendMessage("/bottlexp is only usable by ingame players!");
                        break;
                    }
                    if (!manager.isBottle(((Player) sender).getInventory().getItemInMainHand())) {
                        sender.sendMessage(ChatColor.RED + "You must be holding an Experience Flask!");
                        break;
                    }
                    int amt = Integer.parseInt(args[0]);
                    ExperienceManager m = new ExperienceManager((Player) sender);
                    if (m.getTotalExperience() < amt)
                        sender.sendMessage(ChatColor.RED + "You don't have enough experience!");
                    else {
                        m.setTotalExperience(m.getTotalExperience() - amt);
                        ItemStack item = ((Player) sender).getInventory().getItemInMainHand();
                        ItemMeta meta = item.getItemMeta();
                        List<String> lore = meta.getLore();
                        int current = Integer.parseInt(lore.get(0).substring((ChatColor.WHITE + "Contains: ").length(),
                                lore.get(0).length() - " xp".length()));
                        lore.set(0, ChatColor.WHITE + "Contains: " + (amt + current) + " xp");
                        meta.setLore(lore);
                        item.setItemMeta(meta);
                        ((Player) sender).getInventory().setItemInMainHand(item);
                    }
                } catch (NumberFormatException ex) {
                    sender.sendMessage(ChatColor.RED + "That's not a number...");
                }
            }
            return false;
        case "enchant":
            if (args.length != 0)
                return false;
            if (sender instanceof Player)
                manager.buyGui((Player) sender);
            else
                sender.sendMessage("/enchant is only usable by ingame players.");
            return true;
        case "enchanter":
            if (args.length != 0)
                return false;
            if (!(sender instanceof Player)) {
                sender.sendMessage("/enchanter is only usable by ingame players.");
                return true;
            }
            NPC enchanter = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER,
                    ChatColor.GOLD + "" + ChatColor.BOLD + "Enchanter");
            enchanter.spawn(((Player) sender).getLocation());
            return true;
        }
        return false;
    }

    @EventHandler
    public void onNPCClick(NPCRightClickEvent e) {
        if (e.getNPC().getName().equals(ChatColor.GOLD + "" + ChatColor.BOLD + "Enchanter"))
            manager.buyGui(e.getClicker());
    }

    @EventHandler
    public void onPlayerUseBow(EntityShootBowEvent e) {
        if (manager.isCorrupt(e.getBow())) {
            e.setCancelled(true);
            return;
        }
        if (e.getEntityType() == EntityType.PLAYER)
            e.getProjectile().setMetadata("DarkRealmEnchants_Arrow_Bow", new ItemStackMeta(this, e.getBow()));
    }

    public static ItemStack getBow(Arrow arrow) {
        if (arrow.hasMetadata("DarkRealmEnchants_Arrow_Bow")) {
            ItemStackMeta meta = (ItemStackMeta) arrow.getMetadata("DarkRealmEnchants_Arrow_Bow");
            return meta.getItem();
        } else
            return null;
    }

    @EventHandler
    public void onClickInventory(InventoryClickEvent e) {
        if ((manager.isBottle(e.getWhoClicked().getItemOnCursor())
                && (manager.isBottle(e.getCurrentItem()) || e.getAction() == InventoryAction.COLLECT_TO_CURSOR))
                || (manager.isBottle(e.getCurrentItem()) && e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY))
            e.setCancelled(true);
        if (e.getSlotType() == SlotType.QUICKBAR && e.getSlot() == 9
                && e.getWhoClicked().getGameMode() != GameMode.CREATIVE)
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryInteract(InventoryInteractEvent e) {
        if (e.getView().getTopInventory() != null && (e.getView().getTopInventory().getType() == InventoryType.ANVIL
                || e.getView().getTopInventory().getType() == InventoryType.ENCHANTING))
            e.setCancelled(true);
    }

    @EventHandler
    public void onDragInventory(InventoryDragEvent e) {
        if (manager.isBottle(e.getOldCursor()))
            e.setCancelled(true);
        if (e.getInventory().getType() == InventoryType.CRAFTING && e.getWhoClicked().getGameMode() != GameMode.CREATIVE
                && e.getRawSlots().contains(45))
            e.setCancelled(true);
        if (e.getView().getTopInventory().getType() == InventoryType.ANVIL
                || e.getView().getTopInventory().getType() == InventoryType.ENCHANTING)
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onAttack(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            if (manager.isCorrupt(((Player) e.getDamager()).getInventory().getItemInMainHand()))
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void onSwapItems(PlayerSwapHandItemsEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerEnchant(EnchantItemEvent e) {
        e.setCancelled(true);
    }

}