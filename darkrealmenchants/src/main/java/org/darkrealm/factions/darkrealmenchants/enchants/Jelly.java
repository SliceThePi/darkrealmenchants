package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Jelly extends DREnchant {

    public static final String NAME = "Jelly";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.MYTHICAL };

    public Jelly() {
        super(NAME, ItemType.LEGGINGS, RARITIES);
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player && hasItem((Player) e.getEntity(), 1) && e.getCause() == DamageCause.FALL)
            e.setCancelled(true);
    }

}