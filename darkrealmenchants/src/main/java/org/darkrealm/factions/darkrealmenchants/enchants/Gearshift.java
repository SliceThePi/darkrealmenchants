package org.darkrealm.factions.darkrealmenchants.enchants;

import java.util.Random;
import java.util.UUID;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Gearshift extends DREnchant {

    public static final String NAME = "Gearshift";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.UNCOMMON, EnchantRarity.RARE, EnchantRarity.MYTHICAL,
            EnchantRarity.LEGENDARY };

    public static final AttributeModifier[] MODIFIERS;

    public static final String ATTRIBUTE = "DarkRealm Gearshift Movement Speed Modifier";

    static {
        Random random = new Random(70150552);
        MODIFIERS = new AttributeModifier[] { modifier(0, randomUUID(random)), modifier(1, randomUUID(random)),
                modifier(2, randomUUID(random)), modifier(3, randomUUID(random)), modifier(4, randomUUID(random)) };
    }

    public static AttributeModifier modifier(int amount, UUID uuid) {
        return new AttributeModifier(uuid, ATTRIBUTE, amount / 5.0, Operation.ADD_SCALAR);
    }

    public Gearshift() {
        super(NAME, ItemType.BOOTS, RARITIES);
    }

    @EventHandler
    public void onPlayerSprint(PlayerToggleSprintEvent e) {
        if (hasItem(e.getPlayer(), 0)) {
            if (e.isSprinting()) {
                replacePlayerModifier(e.getPlayer(), Attribute.GENERIC_MOVEMENT_SPEED, ATTRIBUTE,
                        MODIFIERS[level(e.getPlayer(), 0)]);
            } else {
                removePlayerModifier(e.getPlayer(), Attribute.GENERIC_MOVEMENT_SPEED, ATTRIBUTE);
            }
        }
    }

}