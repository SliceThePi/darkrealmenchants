package org.darkrealm.factions.darkrealmenchants;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public enum EnchantRarity {
    COMMON("" + ChatColor.GREEN + ChatColor.BOLD + "Common", 500), UNCOMMON(
            "" + ChatColor.WHITE + ChatColor.BOLD + "Uncommon",
            1000), RARE("" + ChatColor.DARK_AQUA + ChatColor.BOLD + "Rare", 2500), MYTHICAL(
                    "" + ChatColor.YELLOW + ChatColor.BOLD + "Mythical",
                    5000), LEGENDARY("" + ChatColor.GOLD + ChatColor.BOLD + "Legendary", 10000);

    public final String displayName;
    public final ItemStack stack;
    public final int cost;

    private EnchantRarity(String displayName, int cost) {
        this.displayName = displayName;
        this.cost = cost;
        stack = new ItemStack(Material.BOOK);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(displayName + " Enchantment");
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(ChatColor.WHITE + "Cost: " + cost + " xp");
        meta.setLore(lore);
        stack.setItemMeta(meta);
    }

}