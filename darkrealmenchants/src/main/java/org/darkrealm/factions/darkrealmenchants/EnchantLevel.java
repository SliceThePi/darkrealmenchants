package org.darkrealm.factions.darkrealmenchants;

public class EnchantLevel {

    public final DREnchant enchant;
    public final int level;

    public EnchantLevel(DREnchant enchant, int level) {
        this.enchant = enchant;
        this.level = level;
    }
}