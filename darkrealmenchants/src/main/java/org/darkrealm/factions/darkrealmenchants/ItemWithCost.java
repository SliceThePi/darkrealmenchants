package org.darkrealm.factions.darkrealmenchants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemWithCost implements PurchasableItem {

    private final ItemStack displayItem;
    private final ItemStack purchaseItem;
    private final int cost;

    public ItemWithCost(ItemStack item, int cost) {
        this.cost = cost;
        purchaseItem = item.clone();
        displayItem = item.clone();
        ItemMeta meta = displayItem.getItemMeta();
        List<String> lore = (meta.getLore() != null ? meta.getLore() : new ArrayList<String>());
        lore.add(0, ChatColor.WHITE + "Cost: " + cost + " xp");
        meta.setLore(lore);
        displayItem.setItemMeta(meta);
    }

    @Override
    public ItemStack getDisplayItem(EnchantingManager manager) {
        return displayItem;
    }

    @Override
    public ItemStack getPurchaseItem(EnchantingManager manager) {
        return purchaseItem;
    }

    @Override
    public int getCost(EnchantingManager manager) {
        return cost;
    }

}