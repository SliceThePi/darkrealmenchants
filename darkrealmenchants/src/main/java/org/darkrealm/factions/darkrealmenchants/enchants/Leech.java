package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Leech extends DREnchant {

    public static final String NAME = "Leech";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.MYTHICAL, EnchantRarity.MYTHICAL,
            EnchantRarity.MYTHICAL, EnchantRarity.LEGENDARY, EnchantRarity.LEGENDARY };

    public Leech() {
        super(NAME, ItemType.ALL_MELEE, RARITIES);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (!e.isCancelled() && e.getCause() == DamageCause.ENTITY_ATTACK && e.getEntity() instanceof Player
                && e.getDamager() instanceof Player && hasItem((Player) e.getDamager(), 4)) {
            Player p = (Player) e.getDamager();
            int lvl = level(p, 4);
            double amt = e.getFinalDamage() * (0.25 + 0.1 * lvl * (Math.random() + 0.5));
            p.setHealth(Math.min(p.getHealth() + amt, p.getMaxHealth()));
        }
    }
}