package org.darkrealm.factions.darkrealmenchants;

import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_9_R1.NBTTagCompound;

public class ItemBuilder {

    private NBTTagCompound compound;

    public ItemBuilder(ItemStack i) {
        this.compound = CraftItemStack.asNMSCopy(i).getTag();
    }

    public ItemBuilder setUnbreakable(boolean unbreakable) {
        NBTTagCompound tag;
        if (compound.hasKeyOfType("tag", 10))
            tag = compound.getCompound("tag");
        else
            tag = new NBTTagCompound();
        if (unbreakable)
            tag.setBoolean("Unbreakable", true);
        else
            tag.remove("Unbreakable");
        if (tag.isEmpty())
            compound.remove("tag");
        else
            compound.set("tag", tag);
        return this;
    }

    public ItemBuilder hideFlags(int flags) {
        NBTTagCompound tag;
        if (compound.hasKeyOfType("tag", 10))
            tag = compound.getCompound("tag");
        else
            tag = new NBTTagCompound();
        if (tag.hasKey("HideFlags"))
            flags |= tag.getInt("HideFlags");
        if (flags != 0)
            tag.setInt("HideFlags", flags);
        else
            tag.remove("HideFlags");
        if (tag.isEmpty())
            compound.remove("tag");
        else
            compound.set("tag", tag);
        return this;
    }

    public ItemBuilder unhideAllFlags() {
        NBTTagCompound tag;
        if (compound.hasKeyOfType("tag", 10))
            tag = compound.getCompound("tag");
        else
            tag = new NBTTagCompound();
        tag.remove("HideFlags");
        if (tag.isEmpty())
            compound.remove("tag");
        else
            compound.set("tag", tag);
        return this;
    }

    public ItemBuilder removeTag(String name) {
        if (compound.hasKeyOfType("tag", 10)) {
            NBTTagCompound tag = compound.getCompound("tag");
            tag.remove(name);
            compound.set("tag", tag);
        }
        return this;
    }

    public ItemBuilder setInt(String name, int val) {
        NBTTagCompound tag;
        if (compound.hasKeyOfType("tag", 10))
            tag = compound.getCompound("tag");
        else
            tag = new NBTTagCompound();
        tag.setInt(name, val);
        compound.set("tag", tag);
        return this;
    }

    public ItemBuilder setString(String name, String val) {
        NBTTagCompound tag;
        if (compound.hasKeyOfType("tag", 10))
            tag = compound.getCompound("tag");
        else
            tag = new NBTTagCompound();
        tag.setString(name, val);
        compound.set("tag", tag);
        return this;
    }

    public ItemStack build() {
        if (compound.hasKeyOfType("tag", 10) && compound.getCompound("tag").isEmpty())
            compound.remove("tag");
        return CraftItemStack.asBukkitCopy(net.minecraft.server.v1_9_R1.ItemStack.createStack(compound));
    }

}