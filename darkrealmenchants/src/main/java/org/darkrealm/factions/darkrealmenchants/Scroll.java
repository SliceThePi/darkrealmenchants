package org.darkrealm.factions.darkrealmenchants;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public enum Scroll {

    PURITY(ChatColor.GREEN.toString() + ChatColor.BOLD + "Purified",
            ChatColor.GREEN.toString() + ChatColor.BOLD + "Purity Scroll", ChatColor.WHITE + "Pick this up and click",
            ChatColor.WHITE + "an item to purify it."), ETERNITY(
                    ChatColor.DARK_GREEN.toString() + ChatColor.BOLD + "Eternal",
                    ChatColor.DARK_GREEN.toString() + ChatColor.BOLD + "Eternity Scroll",
                    ChatColor.WHITE + "Pick this up and click", ChatColor.WHITE + "an item to preserve it."), ARCHIVE(
                            ChatColor.GOLD.toString() + ChatColor.BOLD + "Archived",
                            ChatColor.GOLD.toString() + ChatColor.BOLD + "Archive Scroll",
                            ChatColor.WHITE + "Pick this up and click", ChatColor.WHITE + "an item to archive it.");

    public final String tag;
    public final ItemStack item;

    private Scroll(String tag, String name, String... lore) {
        this.tag = tag;
        item = new ItemStack(Material.ENCHANTED_BOOK);
        ItemMeta meta = item.getItemMeta();
        ArrayList<String> l = new ArrayList<String>();
        for (String s : lore)
            l.add(s);
        meta.setLore(l);
        meta.setDisplayName(name);
        item.setItemMeta(meta);
    }

}