package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Bulwark extends DREnchant {

    public static final String NAME = "Bulwark";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.MYTHICAL };

    public Bulwark() {
        super(NAME, ItemType.CHESTPLATE, RARITIES);
    }

    @Override
    public void applyDefenseEffect(LivingEntity user, LivingEntity target, int level, EntityDamageEvent e) {
        if (!e.isCancelled() && target instanceof Player
                && (e.getCause() == DamageCause.BLOCK_EXPLOSION || e.getCause() == DamageCause.ENTITY_EXPLOSION))
            e.setCancelled(true);
    }
}