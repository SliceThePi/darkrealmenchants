package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;
import org.darkrealm.factions.darkrealmenchants.TickingEnchant;

public class Clarity extends DREnchant implements TickingEnchant {

    public static final String NAME = "Clarity";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.MYTHICAL, EnchantRarity.MYTHICAL,
            EnchantRarity.MYTHICAL };

    public Clarity() {
        super(NAME, ItemType.HELMET, RARITIES);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerDamage(EntityDamageEvent e) {
        if (!e.isCancelled() && (e.getCause() == DamageCause.POISON || e.getCause() == DamageCause.WITHER)
                && e.getEntity() instanceof Player && hasItem((Player) e.getEntity(), 3)) {
            if (Math.random() < level((Player) e.getEntity(), 3) * 0.25)
                e.setCancelled(true);
        }
    }

    public void tick() {
        for (Player p : Bukkit.getServer().getOnlinePlayers())
            if (hasItem(p, 3)) {
                PotionEffect blindness = null;
                boolean nausea = false;
                for (PotionEffect effect : p.getActivePotionEffects())
                    if (effect.getType() == PotionEffectType.BLINDNESS) {
                        blindness = new PotionEffect(effect.getType(), effect.getDuration() - level(p, 3),
                                effect.getAmplifier());
                    } else if (effect.getType() == PotionEffectType.CONFUSION) {
                        nausea = true;
                    }
                if (blindness != null)
                    p.addPotionEffect(blindness, true);
                if (nausea)
                    p.removePotionEffect(PotionEffectType.CONFUSION);
            }
    }
}