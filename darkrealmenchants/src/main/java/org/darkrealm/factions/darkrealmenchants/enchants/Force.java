package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.DarkRealmEnchants;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

import com.rit.sucy.EnchantmentAPI;

public class Force extends DREnchant {

    public static final String NAME = "Force";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.COMMON, EnchantRarity.COMMON, EnchantRarity.UNCOMMON,
            EnchantRarity.RARE, EnchantRarity.MYTHICAL, EnchantRarity.LEGENDARY };

    public Force() {
        super(NAME, ItemType.BOW, RARITIES);
    }

    public void onEntityDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Arrow && e.getEntity() instanceof LivingEntity && EnchantmentAPI
                .getAllEnchantments(DarkRealmEnchants.getBow((Arrow) e.getDamager())).containsKey(this)) {
            double v = Math.random();
            v = 2 * v * v
                    * EnchantmentAPI.getAllEnchantments(DarkRealmEnchants.getBow((Arrow) e.getDamager())).get(this);
            // push the entity away from the attacker with velocity equal to v.
            Vector knockback = ((Arrow) e.getDamager()).getLocation().getDirection().clone().normalize().multiply(v);
            knockback.setY(0.1);
            e.getEntity().setVelocity(e.getEntity().getVelocity().add(knockback));
        }
    }

}