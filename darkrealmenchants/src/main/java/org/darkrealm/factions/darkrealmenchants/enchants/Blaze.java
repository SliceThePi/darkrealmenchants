package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Blaze extends DREnchant {

	public static final String NAME = "Blaze";

	public static final EnchantRarity[] RARITIES = { EnchantRarity.COMMON, EnchantRarity.UNCOMMON };

	public Blaze() {
		super(NAME, ItemType.ALL_MELEE, RARITIES);
	}

	@Override
	public void applyEffect(LivingEntity user, LivingEntity target, int level, EntityDamageByEntityEvent e) {
		if (!e.isCancelled() && user instanceof Player && hasItem((Player) user, 4)) {
			if (!(target instanceof Player) || Math.random() < 0.2 * level((Player) user, 4))
				e.getEntity().setFireTicks((int) (80 * (level((Player) user, 4) * (Math.random() / 2 + 0.75))));
		}
	}
}