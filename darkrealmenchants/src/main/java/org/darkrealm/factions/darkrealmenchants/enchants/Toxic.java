package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Toxic extends DREnchant {

	public static final String NAME = "Toxic";

	public static final EnchantRarity[] RARITIES = { EnchantRarity.RARE, EnchantRarity.MYTHICAL };

	public Toxic() {
		super(NAME, ItemType.CHESTPLATE, RARITIES);
	}

	@EventHandler
	public void onPlayerTakeDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof LivingEntity
				&& hasItem((Player) e.getEntity(), 2) && Math.random() < 0.1 * level((Player) e.getEntity(), 2)) {
			((LivingEntity) e.getDamager()).addPotionEffect(new PotionEffect(PotionEffectType.POISON,
					40 + (int) (20 * Math.random() * level((Player) e.getEntity(), 2)), 0));
		}
	}

}