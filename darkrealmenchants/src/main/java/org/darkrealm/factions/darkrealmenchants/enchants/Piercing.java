package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.DarkRealmEnchants;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

import com.rit.sucy.EnchantmentAPI;

public class Piercing extends DREnchant {

    public static final String NAME = "Piercing";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.COMMON, EnchantRarity.COMMON, EnchantRarity.UNCOMMON,
            EnchantRarity.RARE, EnchantRarity.MYTHICAL, EnchantRarity.LEGENDARY };

    public Piercing() {
        super(NAME, ItemType.BOW, RARITIES);
    }

    public void onEntityDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Arrow && e.getEntity() instanceof LivingEntity && EnchantmentAPI
                .getAllEnchantments(DarkRealmEnchants.getBow((Arrow) e.getDamager())).containsKey(this)) {
            e.setDamage(e.getDamage()
                    * (1 + EnchantmentAPI.getAllEnchantments(DarkRealmEnchants.getBow((Arrow) e.getDamager())).get(this)
                            / 3.0));
        }
    }

}