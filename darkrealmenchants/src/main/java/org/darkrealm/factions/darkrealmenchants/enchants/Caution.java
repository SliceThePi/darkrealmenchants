package org.darkrealm.factions.darkrealmenchants.enchants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

import net.md_5.bungee.api.ChatColor;

public class Caution extends DREnchant {

    public static final String NAME = "Caution";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.LEGENDARY };

    public static final String PREFIX = ChatColor.GRAY + "Mob type: ";

    public static final ArrayList<Material> BLOCKS = new ArrayList<Material>();

    static {
        BLOCKS.add(Material.GRASS);
        BLOCKS.add(Material.DIRT);
    }

    public Caution() {
        super(NAME, ItemType.PICKAXE, RARITIES);
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBreakBlock(BlockBreakEvent e) {
        if (!e.isCancelled()) {
            Material m = e.getBlock().getType();
            if (BLOCKS.contains(m))
                if (e.getBlock().getType().equals(Material.MOB_SPAWNER)) {
                    CreatureSpawner block = (CreatureSpawner) e.getBlock();
                    ItemStack spawner = new ItemStack(Material.MOB_SPAWNER);
                    ItemMeta meta = spawner.getItemMeta();
                    ArrayList<String> lore = new ArrayList<String>();
                    lore.add(PREFIX + block.getSpawnedType().getName());
                    meta.setLore(lore);
                    spawner.setItemMeta(meta);
                    e.getPlayer().getWorld().dropItemNaturally(e.getBlock().getLocation(), spawner);
                    e.setCancelled(true);
                    e.getBlock().setType(Material.AIR);
                } else if (m.toString().endsWith("_ORE")) {
                    e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(),
                            new ItemStack(e.getBlock().getType()));
                    e.setCancelled(true);
                    e.getBlock().setType(Material.AIR);
                }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerPlaceBlock(BlockPlaceEvent e) {
        if (!e.isCancelled() && e.getItemInHand() != null && e.getItemInHand().getType() == Material.MOB_SPAWNER) {
            CreatureSpawner cs = (CreatureSpawner) e.getBlock();
            if (e.getItemInHand().getItemMeta() != null && e.getItemInHand().getItemMeta().getLore() != null) {
                List<String> lore = e.getItemInHand().getItemMeta().getLore();
                EntityType type = EntityType.PIG;
                for (EntityType t : EntityType.values())
                    if (lore.contains(PREFIX + t.getName())) {
                        type = t;
                        break;
                    }
                cs.setSpawnedType(type);
            }
        }
    }

}