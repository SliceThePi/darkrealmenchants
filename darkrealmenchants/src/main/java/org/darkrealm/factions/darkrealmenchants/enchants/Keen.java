package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Keen extends DREnchant {

    public static final String NAME = "Keen";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.COMMON, EnchantRarity.COMMON, EnchantRarity.UNCOMMON,
            EnchantRarity.RARE, EnchantRarity.MYTHICAL, EnchantRarity.LEGENDARY };

    public Keen() {
        super(NAME, ItemType.ALL_MELEE, RARITIES);
    }

    @Override
    public void applyEffect(LivingEntity user, LivingEntity target, int level, EntityDamageByEntityEvent e) {
        if (!e.isCancelled() && user instanceof Player && hasItem((Player) user, 4))
            e.setDamage(e.getDamage() * (2 * level / 1.5));
    }
}