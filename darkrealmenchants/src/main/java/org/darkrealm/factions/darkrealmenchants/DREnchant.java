package org.darkrealm.factions.darkrealmenchants;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.rit.sucy.CustomEnchantment;
import com.rit.sucy.EnchantmentAPI;

public abstract class DREnchant extends CustomEnchantment implements Listener {

    private final EnchantRarity[] rarities;

    public final ItemType appliesTo;

    public static enum ItemType {
        BOOTS("boots", ".*_BOOTS$"),
        LEGGINGS("leggings", ".*_LEGGINGS$"),
        CHESTPLATE("chestplate", ".*_CHESTPLATE$"),
        HELMET("helmet", ".*_HELMET$"),
        SWORD("sword", ".*_SWORD$"),
        AXE("axe", ".*_AXE$"),
        PICKAXE("pickaxe", ".*PICKAXE$"),
        BOW("bow", "^BOW$"),
        SPADE("spade", ".*SPADE$"),
        ALL_MELEE("melee weapon", AXE, SWORD),
        ALL_WEAPONS("weapon", ALL_MELEE, BOW),
        ALL_ARMOR("armor", BOOTS, LEGGINGS, CHESTPLATE, HELMET),
        DIGGING("digging tool", SPADE, PICKAXE);

        public final ArrayList<Material> materials = new ArrayList<Material>();
        public final String name;

        private ItemType(String name, String regex) {
            this.name = name;
            for (Material m : Material.values())
                if (m.name().matches(regex))
                    materials.add(m);
        }

        private ItemType(String name, ItemType... children) {
            this.name = name;
            for (ItemType it : children)
                for (Material m : it.materials)
                    materials.add(m);
        }
    };

    public DREnchant(String name, ItemType appliesTo, EnchantRarity[] rarities) {
        super(name, appliesTo.materials.toArray(new Material[] {}), 0);
        this.appliesTo = appliesTo;
        this.rarities = rarities;
        setMaxLevel(rarities.length);
    }

    public boolean hasItem(Player p, int slot) {
        ItemStack is = slotToItem(p, slot);
        if (is == null)
            return false;
        return EnchantmentAPI.itemHasEnchantment(is, this.name());
    }

    public ItemStack slotToItem(Player p, int slot) {
        switch (slot) {
        case 0:
            return p.getInventory().getBoots();
        case 1:
            return p.getInventory().getLeggings();
        case 2:
            return p.getInventory().getChestplate();
        case 3:
            return p.getInventory().getHelmet();
        case 4:
            return p.getInventory().getItemInMainHand();
        case 5:
            return p.getInventory().getItemInOffHand();
        default:
            return null;
        }
    }

    public int level(Player p, int slot) {
        ItemStack is = slotToItem(p, slot);
        if (is == null)
            return 0;
        Map<CustomEnchantment, Integer> m = EnchantmentAPI.getAllEnchantments(is);
        if (m == null || !m.containsKey(this))
            return 0;
        if (m.get(this) > getMaxLevel())
            return getMaxLevel();
        return m.get(this);
    }

    public EnchantRarity[] rarities() {
        return rarities;
    }

    public boolean playerHasModifier(Player p, Attribute attribute, String modifier) {
        for (AttributeModifier am : p.getAttribute(attribute).getModifiers())
            if (am.getName().equals(modifier))
                return true;
        return false;
    }

    public void removePlayerModifier(Player p, Attribute attribute, String modifier) {
        ArrayList<AttributeModifier> alam = new ArrayList<AttributeModifier>();
        AttributeInstance ai = p.getAttribute(attribute);
        for (AttributeModifier am : ai.getModifiers())
            if (am.getName().equals(modifier))
                alam.add(am);
        for (AttributeModifier am : alam)
            ai.removeModifier(am);
    }

    public void replacePlayerModifier(Player p, Attribute attribute, String modifier, AttributeModifier newMod) {
        removePlayerModifier(p, attribute, modifier);
        p.getAttribute(attribute).addModifier(newMod);
    }

    public void addPlayerModifier(Player p, Attribute attribute, AttributeModifier newMod) {
        p.getAttribute(attribute).addModifier(newMod);
    }

    public static UUID randomUUID(Random random) {
        return new UUID(random.nextLong(), random.nextLong());
    }

}