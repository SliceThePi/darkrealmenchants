package org.darkrealm.factions.darkrealmenchants;

import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class ScrollBook implements PurchasableItem {

    public final Scroll scroll;
    private final ItemStack displayItem;
    private final int cost;

    public ScrollBook(Scroll scroll, int cost) {
        this.scroll = scroll;
        this.cost = cost;
        displayItem = scroll.item.clone();
        ItemMeta meta = displayItem.getItemMeta();
        List<String> lore = meta.getLore();
        lore.add(0, ChatColor.WHITE + "Cost: " + cost + " xp");
        meta.setLore(lore);
        displayItem.setItemMeta(meta);
    }

    @Override
    public ItemStack getDisplayItem(EnchantingManager manager) {
        return displayItem;
    }

    @Override
    public ItemStack getPurchaseItem(EnchantingManager manager) {
        return scroll.item;
    }

    @Override
    public int getCost(EnchantingManager manager) {
        return cost;
    }

}