package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.Material;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Decapitation extends DREnchant {

    public static final String NAME = "Decapitation";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.UNCOMMON };

    public Decapitation() {
        super(NAME, ItemType.ALL_MELEE, RARITIES);
    }

    public void onPlayerDeath(PlayerDeathEvent e) {
        if (e.getEntity().getKiller() != null && hasItem(e.getEntity().getPlayer(), 4) && Math.random() < 0.5) {
            ItemStack s = new ItemStack(Material.SKULL_ITEM);
            SkullMeta m = (SkullMeta) s.getItemMeta();
            m.setOwner(e.getEntity().getName());
            s.setItemMeta(m);
            e.getDrops().add(s);
        }
    }
}