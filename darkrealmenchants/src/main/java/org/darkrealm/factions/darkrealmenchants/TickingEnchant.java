package org.darkrealm.factions.darkrealmenchants;

public interface TickingEnchant {
    public void tick();
}