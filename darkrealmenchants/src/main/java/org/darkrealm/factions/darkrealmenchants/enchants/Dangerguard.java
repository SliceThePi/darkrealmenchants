package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Dangerguard extends DREnchant {

    public static final String NAME = "Dangerguard";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.COMMON, EnchantRarity.COMMON, EnchantRarity.UNCOMMON,
            EnchantRarity.UNCOMMON, EnchantRarity.RARE, EnchantRarity.RARE, EnchantRarity.MYTHICAL,
            EnchantRarity.MYTHICAL, EnchantRarity.LEGENDARY, EnchantRarity.LEGENDARY };

    public Dangerguard() {
        super(NAME, ItemType.ALL_ARMOR, RARITIES);
    }

    @Override
    public void applyDefenseEffect(LivingEntity user, LivingEntity target, int level, EntityDamageEvent e) {
        if (!e.isCancelled() && target instanceof Player && e.getCause() != DamageCause.VOID)
            e.setDamage(e.getDamage() * Math.pow(0.89595845984, level));
    }
}