package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Obliteration extends DREnchant {

	public static final String NAME = "Obliteration";

	public static final EnchantRarity[] RARITIES = { EnchantRarity.UNCOMMON, EnchantRarity.RARE };

	public Obliteration() {
		super(NAME, ItemType.AXE, RARITIES);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerDamage(EntityDamageByEntityEvent e) {
		if (!e.isCancelled() && e.getEntity() instanceof LivingEntity && e.getDamager() instanceof Player
				&& hasItem((Player) e.getDamager(), 4) && Math.random() < 0.5) {
			double v = Math.random();
			v = 2 * v * v * level((Player) e.getDamager(), 4);
			// push the entity away from the attacker with velocity equal to v.
			Vector knockback = e.getEntity().getLocation().clone().subtract(e.getDamager().getLocation()).toVector()
					.normalize().multiply(v);
			knockback.setY(0.1);
			e.getEntity().setVelocity(e.getEntity().getVelocity().add(knockback));
		}
	}

}