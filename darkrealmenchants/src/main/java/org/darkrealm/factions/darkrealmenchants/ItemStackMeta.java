package org.darkrealm.factions.darkrealmenchants;

import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

public class ItemStackMeta extends FixedMetadataValue {

    public ItemStackMeta(Plugin owningPlugin, ItemStack value) {
        super(owningPlugin, value);
    }

    public ItemStack getItem() {
        return (ItemStack) this.value();
    }

}