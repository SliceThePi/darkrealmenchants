package org.darkrealm.factions.darkrealmenchants.enchants;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Opulent extends DREnchant {

    public static final String NAME = "Opulent";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.COMMON, EnchantRarity.COMMON, EnchantRarity.UNCOMMON,
            EnchantRarity.RARE, EnchantRarity.MYTHICAL, EnchantRarity.LEGENDARY };

    public static final HashMap<Material, Material> BLOCK_TYPES_DROPS = new HashMap<Material, Material>();

    static {
        BLOCK_TYPES_DROPS.put(Material.COAL_ORE, Material.COAL);
        BLOCK_TYPES_DROPS.put(Material.DIAMOND_ORE, Material.DIAMOND);
        BLOCK_TYPES_DROPS.put(Material.EMERALD_ORE, Material.EMERALD);
        BLOCK_TYPES_DROPS.put(Material.QUARTZ_ORE, Material.QUARTZ);
    }

    private final Clarity clarity;

    public Opulent(Clarity clarity) {
        super(NAME, ItemType.PICKAXE, RARITIES);
        this.clarity = clarity;
    }

    public void onBlockBreak(BlockBreakEvent e) {
        if (!clarity.hasItem(e.getPlayer(), 4) && hasItem(e.getPlayer(), 4)
                && BLOCK_TYPES_DROPS.containsKey(e.getBlock().getType())) {
            e.getPlayer().getWorld().dropItemNaturally(e.getBlock().getLocation(),
                    new ItemStack(BLOCK_TYPES_DROPS.get(e.getBlock().getType()), level(e.getPlayer(), 4) + 1));
        }
    }
}