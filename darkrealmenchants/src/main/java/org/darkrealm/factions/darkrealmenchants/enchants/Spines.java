package org.darkrealm.factions.darkrealmenchants.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.darkrealm.factions.darkrealmenchants.DREnchant;
import org.darkrealm.factions.darkrealmenchants.EnchantRarity;

public class Spines extends DREnchant {

    public static final String NAME = "Spines";

    public static final EnchantRarity[] RARITIES = { EnchantRarity.UNCOMMON, EnchantRarity.RARE };

    public Spines() {
        super(NAME, ItemType.ALL_ARMOR, RARITIES);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (!e.isCancelled() && e.getCause() == DamageCause.ENTITY_ATTACK && e.getEntity() instanceof Player
                && e.getDamager() instanceof LivingEntity && Math.random() < 0.5) {
            double damage = 0.0;
            for (int i = 0; i < 4; i++) {
                damage += (Math.random() + 0.5) * 0.1 * level((Player) e.getEntity(), i);
            }
            ((LivingEntity) e.getDamager()).damage(damage);
        }
    }
}